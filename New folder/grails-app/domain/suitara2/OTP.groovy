package suitara2

class OTP
{
    String mobile_otp
    String email_otp

    Date mobile_otp_generation_date
    Date mobile_otp_expiry_date

    Date email_otp_generation_date
    Date email_otp_expiry_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
    }
}

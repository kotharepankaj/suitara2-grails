package suitara2
import grails.gorm.MultiTenant
class Stock implements MultiTenant<Stock>
{
    String stock_id
    String name
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static hasMany = [store: Store,source:Source]
    static constraints = {
        name nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

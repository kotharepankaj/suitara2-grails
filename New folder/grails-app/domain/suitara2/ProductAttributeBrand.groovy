package suitara2
import grails.gorm.MultiTenant
class ProductAttributeBrand implements MultiTenant<ProductAttributeBrand>
{
    boolean isactive
    boolean is_brand_applicable
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,brand:Brand,productattribute:ProductAttribute]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

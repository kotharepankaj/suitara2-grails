package suitara2
import grails.gorm.MultiTenant
class CategoryComponent implements MultiTenant<CategoryComponent>
{
    String name  //Jacket/Trouser
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,category:Category]
    static constraints = {
        name nullable:true
        category nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

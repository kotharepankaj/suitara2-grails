package suitara2
import grails.gorm.MultiTenant
class Person implements MultiTenant<Person>
{
    String firstname
    String lastname
    String middlename
    String fullname
    Date date_of_birth
    String pofile_pic_path
    String pofile_pic_name


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[user:User,gender:Gender,language:Language,country:Country,organization:Organization]
    static constraints = {
        firstname nullable:true
        lastname nullable:true
        middlename nullable:true
        fullname nullable:true
        date_of_birth nullable:true
        pofile_pic_path nullable:true
        pofile_pic_name nullable:true
        gender nullable:true
        language nullable:true
    }
}

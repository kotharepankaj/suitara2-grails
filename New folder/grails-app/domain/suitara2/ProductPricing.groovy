package suitara2
import grails.gorm.MultiTenant
class ProductPricing implements MultiTenant<ProductPricing>
{
    double price
    double msrp  //(Manufacturer’s Suggested Retail Price)
    double minimum_advertisement_price
    double special_price
    Date special_price_from
    Date special_price_to
    double quantity
    double discount_in_percentage
    double extended_warrenty_price
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,priceregion:PriceRegion,product:Product,pricetype:PriceType]
    static constraints = {
        special_price_from nullable:true
        special_price_to nullable:true
        priceregion nullable:true
        pricetype nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        price defaultValue: 0
        msrp defaultValue: 0
        minimum_advertisement_price defaultValue: 0
        special_price defaultValue: 0
        quantity defaultValue: 0
        discount_in_percentage defaultValue: 0
        extended_warrenty_price defaultValue: 0
    }
}

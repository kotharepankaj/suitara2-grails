package suitara2
import grails.gorm.MultiTenant
class Address implements MultiTenant<Address>
{
    String name
    String address
    String street
    String city
    String company
    boolean isdefault
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,addresstype:AddressType,person:Person,state:State,
                      country:Country,pin:Pin,order:Order]
    static constraints = {
        name nullable:true
        address nullable:true
        street nullable:true
        city nullable:true
        company nullable:true
        order nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        isdefault defaultValue: true
    }
}

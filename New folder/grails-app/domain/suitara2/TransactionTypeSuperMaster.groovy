package suitara2

class TransactionTypeSuperMaster {

    String name  //Order/Authorization/Capture/Void/Refund
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class ProductImages implements MultiTenant<ProductImages>
{
    String name  //front/back/full
    int sort_order
    boolean isactive
    String filename
    String filepath

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,product:Product,categoryimageposition:CategoryImagePosition]
    static constraints = {
        categoryimageposition nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

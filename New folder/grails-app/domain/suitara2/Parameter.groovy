package suitara2
import grails.gorm.MultiTenant
class Parameter implements MultiTenant<Parameter>
{
    String name   //Height/Weight/Body Shape/Backsection
    boolean isactive

    boolean ispropertiesapplicable
    double min_value
    double max_value
    String video_file_path
    String video_file_name

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,categorycomponent:CategoryComponent,category:Category]
    static constraints = {
        video_file_path nullable:true
        video_file_name nullable:true
        categorycomponent nullable:true
        category nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        ispropertiesapplicable defaultValue: false
        min_value defaultValue: 0
        max_value defaultValue: 0
    }
}

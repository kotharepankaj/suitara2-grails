package suitara2
import grails.gorm.MultiTenant
class User implements MultiTenant<User>
{
    String username    //Login username
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,usertype:UserType]
    static hasMany = [roles: Role]
    static constraints = {
        organization nullable:true
        usertype nullable:true
    }
}

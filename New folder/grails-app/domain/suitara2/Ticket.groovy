package suitara2
import grails.gorm.MultiTenant
class Ticket implements MultiTenant<Ticket>
{
    String ticket_id
    Date start_date
    Date close_date
    String description
    String remark
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,product:Product,
                      tickettype:TicketType,assignedto:Employee,ticketstatus:TicketStatus]
    static constraints = {
        start_date nullable:true
        close_date nullable:true
        description nullable:true
        remark nullable:true
        user nullable:true
        product nullable:true
        tickettype nullable:true
        assignedto nullable:true
        ticketstatus nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

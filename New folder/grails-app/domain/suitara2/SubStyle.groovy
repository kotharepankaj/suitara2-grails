package suitara2
import grails.gorm.MultiTenant
class SubStyle implements MultiTenant<SubStyle>
{
    String name        // Jacket Type / Lapel / Pockets
    String filename
    String filepath
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,category:Category,style:Style]
    static constraints = {
        filename nullable:true
        filepath nullable:true
        category nullable:true
        style nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

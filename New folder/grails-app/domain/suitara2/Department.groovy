package suitara2
import grails.gorm.MultiTenant
class Department implements MultiTenant<Department>
{
    String name  //customer/admin/marketing/sales/storekeeper/accounts/tailor
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        organization nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class Shipment implements MultiTenant<Shipment>
{
    boolean isactive
    String shipment_id
    Date shipment_date
    String shipping_information

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,customer:Customer,
                      order:Order,product:Product,invoice: Invoice,shipmentstatus: ShipmentStatus]
    static hasMany = [pickup: Pickup]
    static constraints = {
        shipment_id nullable:true
        shipment_date nullable: true
        shipping_information nullable:true
        order nullable:true
        product nullable:true
        invoice nullable:true
        shipmentstatus nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class Dispatch implements MultiTenant<Dispatch>
{
    boolean isactive

    Date created_at
    Date ready_at
    double total_shipments
    double failed_shipments
    Date ready_for_dispatch_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,customer:Customer,order:Order,product:Product,
                      invoice:Invoice,shipment:Shipment,pickup:Pickup,dispatchstatus:DispatchStatus,
                      carrier:Carrier,dispatchlocation:DispatchLocation]
    static constraints = {
        customer nullable:true
        order nullable:true
        product nullable:true
        invoice nullable:true
        shipment nullable:true
        pickup nullable:true
        dispatchstatus nullable:true
        carrier nullable:true
        dispatchlocation nullable:true
        created_at nullable:true
        ready_at nullable:true
        ready_for_dispatch_date nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        total_shipments defaultValue: 0
        failed_shipments defaultValue: 0
    }
}

package suitara2
import grails.gorm.MultiTenant
class CustomerGroup implements MultiTenant<CustomerGroup>
{
    String name         //Not Logged In, Registered/xyz
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class Currency implements MultiTenant<Currency>
{
    String name       //Rupees/Dollar
    String description
    String symbol
    double conversion
    String region
    String time_diff
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,country:Country]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

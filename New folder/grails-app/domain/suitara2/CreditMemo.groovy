package suitara2
import grails.gorm.MultiTenant
class CreditMemo implements MultiTenant<CreditMemo>
{
    String credit_memo_id
    Date refund_date
    double refund_amount
    double adjustment_fee
    double grand_total
    Date invoice_date

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,customer:Customer,order:Order,product:Product,
                      creditmemostatus:CreditMemoStatus,invoice:Invoice]
    static constraints = {
        credit_memo_id nullable:true
        refund_date nullable:true
        invoice_date nullable:true
        customer nullable:true
        order nullable:true
        product nullable:true
        creditmemostatus nullable:true
        invoice nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        refund_amount defaultValue: 0
        adjustment_fee defaultValue: 0
        grand_total defaultValue: 0
    }
}

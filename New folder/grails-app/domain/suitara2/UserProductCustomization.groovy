package suitara2
import grails.gorm.MultiTenant
class UserProductCustomization implements MultiTenant<UserProductCustomization>
{
    Date customization_date
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,product:Product,cart:Cart,
                      category:Category,style:Style,substyle:SubStyle]
    static constraints = {
        customization_date nullable:true
        cart nullable:true
        category nullable:true
        style nullable:true
        substyle nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class Employee implements MultiTenant<Employee>
{
    boolean isactive
    String designation

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,person:Person,department:Department]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class ComponentStandardSize implements MultiTenant<ComponentStandardSize>
{
    String name   //Extra Small/Small/Medium/Large/Extra Large/2XL/3XL
    int sort_order
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,category:Category,categorycomponent:CategoryComponent]
    static constraints = {
        name nullable:true
        categorycomponent nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        sort_order defaultValue: 0
    }
}

package suitara2
import grails.gorm.MultiTenant
class Cart implements MultiTenant<Cart>
{
    int sort_order
    Date entry_date
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,product:Product]
    static constraints = {
        entry_date nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        sort_order defaultValue: 0
    }
}

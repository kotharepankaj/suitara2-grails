package suitara2
import grails.gorm.MultiTenant
class Invoice implements MultiTenant<Invoice>
{
    boolean isactive
    String invoice_id
    Date invoice_date
    double price
    double quantity
    double discount
    double sub_total
    double tax
    double  shipping_and_handling_charges
    double shipping_tax
    double grand_total
    double total_refunded

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,order:Order,customer:Customer,invoicestatus:InvoiceStatus,
                      product:Product,priceregion:PriceRegion,paymentmethod:PaymentMethod]
    static constraints = {
        invoice_id nullable:true
        invoice_date nullable:true
        shipping_information nullable:true
        invoicestatus nullable:true
        priceregion nullable:true
        paymentmethod nullable:true

    }
    static mapping = {
        isactive defaultValue: true
        price nullable:true
        quantity nullable:true
        discount nullable:true
        sub_total nullable:true
        tax nullable:true
        shipping_and_handling_charges nullable:true
        shipping_tax nullable:true
        grand_total nullable:true
        total_refunded nullable:true
    }
}

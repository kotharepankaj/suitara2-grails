package suitara2
import grails.gorm.MultiTenant
class Gender implements MultiTenant<Gender>
{
    String name  //GenderSuperMaster //male,female
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class Transaction implements MultiTenant<Transaction>
{
    boolean isactive
    String transaction_id
    Date transaction_initiation_date
    Date transaction_completion_date
    double amount
    double transaction_charges
    double total
    boolean isclosed   // Yes/No

    double fraudlabspro_score
    String fraudlabspro_message
    String fraudlabspro_status


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,paymentmethod:PaymentMethod,order:Order,
                      invoice:Invoice,transactiontype:TransactionType,customer:Customer,
                      paymentstatus:PaymentStatus,currency:Currency,paymentgateway:PaymentGateway]
    static constraints = {
        paymentmethod nullable:true
        transactiontype nullable:true
        paymentstatus nullable:true
        transaction_initiation_date nullable:true
        transaction_completion_date nullable:true
        fraudlabspro_message nullable:true
        fraudlabspro_status nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        isclosed defaultValue: 0
        amount defaultValue: 0
        transaction_charges defaultValue: 0
        total defaultValue: 0
        fraudlabspro_score defaultValue: 0
    }
}

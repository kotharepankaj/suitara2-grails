package suitara2

class LinkSuperMaster {

    int linkid
    String controller_name   //component_name
    String action_name   // activity/call
    String link_name
    String link_display_name
    String icon
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

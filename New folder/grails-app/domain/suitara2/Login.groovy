package suitara2

class Login
{
    String username   //100@vit.edu
    String mobile
    String email
    String password   //encrypted password
    String token   //access token to hit API, static mapping should be text
    Date tokenexpiry
    boolean isloginblocked
    boolean is_policy_read_accepted
    String logindevice
    Date lastlogindate
    String google_id
    String facebook_id
    boolean is_email_verified
    boolean is_mobile_verified


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[tenantclient:TenantClient,apptype:AppType]
    static constraints = {
        tenantclient nullable:true
        token nullable:true
        tokenexpiry nullable:true
        logindevice nullable:true
        apptype nullable:true
        mobile nullable:true
        email nullable:true
        logindevice nullable:true
        google_id nullable:true
        facebook_id nullable:true
    }

    static mapping = {
        token type:'text'
        isloginblocked defaultValue:false
        is_policy_read_accepted defaultValue:false
        is_email_verified defaultValue:false
        is_mobile_verified defaultValue:false
    }
}

package suitara2
import grails.gorm.MultiTenant
class Role implements MultiTenant<Role>
{
    String name  //customer/admin/marketing/sales/storekeeper/accounts/tailor
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,usertype:UserType]
    static constraints = {
        organization nullable:true
        usertype nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

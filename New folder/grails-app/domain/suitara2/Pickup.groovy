package suitara2
import grails.gorm.MultiTenant
class Pickup implements MultiTenant<Pickup>
{
    boolean isactive
    String pickup_id
    String origin_location
    Date ready_for_pickup_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,customer:Customer,
                      order:Order,product:Product,invoice:Invoice,shipment:Shipment,pickupstatus:PickupStatus]
    static constraints = {
        customer nullable:true
        order nullable:true
        product nullable:true
        invoice nullable:true
        shipment nullable:true
        pickupstatus nullable:true
        pickup_id nullable:true
        origin_location nullable:true
        ready_for_pickup_date nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class TaxRule implements MultiTenant<TaxRule>
{
    String tax_name
    double tax_rate
    int priority
    int sortorder
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[taxcustomertype:TaxCustomerType,taxproducttype:TaxProductType,
                      taxclasstype:TaxClassType,taxclasssubtype:TaxClassSubType,country:Country, state:State]
    static constraints = {
        tax_name nullable:true
        taxcustomertype nullable:true
        taxproducttype nullable:true
        taxclasstype nullable:true
        taxclasssubtype nullable:true
        country nullable:true
        state nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        tax_rate defaultValue: 0
        priority defaultValue: 0
        sortorder defaultValue: 0
    }
}

package suitara2
import grails.gorm.MultiTenant
class ProductAttribute implements MultiTenant<ProductAttribute>
{
    String name        // Red/Blue/Green/100% wool / 70% wool/ Plain/Checks/XXL/XL
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,productattributetype:ProductAttributeType]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

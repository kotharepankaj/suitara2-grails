package suitara2
import grails.gorm.MultiTenant
class ParameterStandardValue implements MultiTenant<ParameterStandardValue>
{
    double value
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,parameter:Parameter,categorycomponent:CategoryComponent,
                      componentstandardsize:ComponentStandardSize,category:Category,
                      parameterproperty:ParameterProperty]
    static constraints = {
        categorycomponent nullable:true
        category nullable:true
        parameterproperty nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

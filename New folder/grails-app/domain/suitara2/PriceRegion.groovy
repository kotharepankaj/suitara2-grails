package suitara2
import grails.gorm.MultiTenant
class PriceRegion implements MultiTenant<PriceRegion>
{
    String name  //India/Other
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,country:Country,currency:Currency]
    static constraints = {
        country nullable:true
        currency nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

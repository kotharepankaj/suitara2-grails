package suitara2
import grails.gorm.MultiTenant
class PlatformDiscount implements MultiTenant<PlatformDiscount>
{
    String name  //Diwali Offer
    boolean isactive
    Date special_price_from
    Date special_price_to
    Double discount_in_percentage

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        name nullable:true
        special_price_from nullable:true
        special_price_to nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        discount_in_percentage defaultValue: 0
    }
}

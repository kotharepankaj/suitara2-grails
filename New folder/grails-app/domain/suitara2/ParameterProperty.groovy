package suitara2
import grails.gorm.MultiTenant
class ParameterProperty implements MultiTenant<ParameterProperty>
{
    String name  //Medium Belly / Flat Belly
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,parameter:Parameter]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class UserProductMeasurement implements MultiTenant<UserProductMeasurement>
{
    double value
    boolean isactive
    Date measurement_date
    String user_measurement_name

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,product:Product,category:Category,
                      categorycomponet:CategoryComponent, parameter:Parameter,
                      parameterproperty:ParameterProperty,cart:Cart]
    static constraints = {
        measurement_date nullable:true
        user_measurement_name nullable:true
        product nullable:true
        category nullable:true
        categorycomponet nullable:true
        parameter nullable:true
        parameterproperty nullable:true
        cart nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        value defaultValue: 0
    }
}

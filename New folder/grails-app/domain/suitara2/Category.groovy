package suitara2
import grails.gorm.MultiTenant
class Category implements MultiTenant<Category>
{
    String name        // Suits/Tuxedos/Shirts
    String description
    boolean includeinmenu
    boolean isactive
    String threed_model_file_path
    String threed_model_file_name

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,rootcategory:RootCategory]
    static constraints = {
    }
    static mapping = {
        includeinmenu defaultValue: true
        isactive defaultValue: true
    }
}

package suitara2
import grails.gorm.MultiTenant
class Organization implements MultiTenant<Organization>
{
    boolean isactive
    String tenantid    //TenantClient
    String tenantname  //TenantClient    //Sales Channel:Suitara
    String name
    String display_name
    String address
    String phone
    String email
    String website
    String country
    String loginattachementdomain   //@vit.edu

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
        name nullable: true
        display_name nullable: true
        address nullable: true
        phone nullable: true
        email nullable: true
        website nullable: true
        country nullable: true
        loginattachementdomain nullable: true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

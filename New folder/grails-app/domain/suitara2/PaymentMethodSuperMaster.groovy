package suitara2

class PaymentMethodSuperMaster
{
    String name  //Cash On Delivery/Online/Card
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

package suitara2

class CurrencySuperMaster {

    String name       //Rupees/Dollar
    String description
    String symbol
    double conversion
    String region
    String time_diff
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[country:CountrySuperMaster]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

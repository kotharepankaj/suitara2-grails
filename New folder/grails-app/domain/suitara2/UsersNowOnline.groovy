package suitara2
import grails.gorm.MultiTenant
class UsersNowOnline implements MultiTenant<UsersNowOnline>
{
    boolean islive
    String ipadress
    Date lastactivity

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        lastactivity nullable:true
        ipadress nullable:true
    }
    static mapping = {
        islive defaultValue: true
    }
}

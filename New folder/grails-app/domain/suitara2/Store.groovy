package suitara2
import grails.gorm.MultiTenant
class Store implements MultiTenant<Store>
{
    String code
    boolean isactive
    String name
    int sort_order
    boolean set_as_default
    String phone_number
    String hours_of_operation
    String city
    String street
    String address
    String tax_number

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,state:State,country:Country,pin:Pin]
    static constraints = {
        code nullable:true
        name nullable:true
        phone_number nullable:true
        hours_of_operation nullable:true
        city nullable:true
        street nullable:true
        address nullable:true
        tax_number nullable:true
        state nullable:true
        country nullable:true
        pin nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        sort_order defaultValue: 0
        set_as_default defaultValue: true
    }
}

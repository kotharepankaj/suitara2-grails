package suitara2
import grails.gorm.MultiTenant
class Product implements MultiTenant<Product>
{
    String name
    String description
    boolean isactive
    String thumbnail_file_path
    String thumbnail_file_name
    String sku   // for instance, SKU for black Nike women running shoes in size 8 can be NIKE-WMN-BLK-8
    String upc   // UPC stands for Universal Product Code and is commonly called barcode. UPC is a global system of 12 (in some cases 13) digits, created more than 40 years ago for trade tracking purposes.
    double out_of_stock_threshold  //5
    double quantity     //100
    double minimum_quantity_allowed_in_cart
    double maximum_quantity_allowed_in_cart
    double weight_in_grams
    Date set_product_new_from
    Date set_product_new_to
    Date active_from
    Date active_to
    double warranty_in_years
    double extended_warranty_in_years
    String meta_title
    String meta_keywords
    String meta_description
    String url_key
    boolean allow_gift_message
    String manufacturer
    double rating


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,producttype:ProductType,stockstatus:StockStatus,
                      productvisibilitysetting:ProductVisibilitySetting,taxproducttype:TaxProductType,
                      category:Category,rootcategory:RootCategory,manufacturingcountry:Country,brand: Brand,
                      installationtype:InstallationType]
    static hasMany = [productimages: ProductImages]
    static constraints = {
        description nullable:true
        thumbnail_file_path nullable:true
        thumbnail_file_name nullable:true
        sku nullable:true
        upc nullable:true
        set_product_new_from nullable:true
        set_product_new_to nullable:true
        active_from nullable:true
        active_to nullable:true
        meta_title nullable:true
        meta_keywords nullable:true
        meta_description nullable:true
        url_key nullable:true
        manufacturer nullable:true
        producttype nullable:true
        stockstatus nullable:true
        productvisibilitysetting nullable:true
        taxproducttype nullable:true
        category nullable:true
        rootcategory nullable:true
        manufacturingcountry nullable:true
        brand nullable:true
        installationtype nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        out_of_stock_threshold defaultValue: 0
        quantity defaultValue: 0
        minimum_quantity_allowed_in_cart defaultValue: 1
        maximum_quantity_allowed_in_cart defaultValue: 3
        weight_in_grams defaultValue: 0
        warranty_in_years defaultValue: 0
        extended_warranty_in_years defaultValue: 0
        allow_gift_message defaultValue: false
        rating defaultValue: 0
    }
}

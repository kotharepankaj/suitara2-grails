package suitara2
import grails.gorm.MultiTenant
class Style implements MultiTenant<Style>
{
    String name        // Jacket Type / Lapel / Pockets
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,category:Category]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

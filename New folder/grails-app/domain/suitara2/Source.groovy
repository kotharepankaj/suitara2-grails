package suitara2
import grails.gorm.MultiTenant
class Source implements MultiTenant<Source>
{
    boolean isactive
    String code
    String name
    String contact_name
    String email
    String latitude
    String longitude
    String city
    String street
    String address
    String phone
    String fax

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,state:State,country:Country,pin:Pin]
    static constraints = {
        code nullable:true
        name nullable:true
        contact_name nullable:true
        email nullable:true
        latitude nullable:true
        longitude nullable:true
        city nullable:true
        street nullable:true
        address nullable:true
        phone nullable:true
        fax nullable:true
        state nullable:true
        country nullable:true
        pin nullable:true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

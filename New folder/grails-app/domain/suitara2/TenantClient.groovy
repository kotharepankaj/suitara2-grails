package suitara2

class TenantClient
{
    String name    //name of tenant -suitara
    String tenantid
    String domainname   //https://suitara.com/
    String logopath
    String logoname
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
        name nullable: true
        tenantid nullable: true
        domainname nullable: true
        logopath nullable: true
        logoname nullable: true
    }
    static mapping = {
        isactive defaultValue: true
    }
}

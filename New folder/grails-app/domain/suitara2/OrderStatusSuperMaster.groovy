package suitara2

class OrderStatusSuperMaster {

    String name  //Complete/Closed/Cancelled/Processing/Pending/Shipped/On Hold
    boolean isactive
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
}

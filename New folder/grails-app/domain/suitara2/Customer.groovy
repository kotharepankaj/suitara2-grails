package suitara2
import grails.gorm.MultiTenant
class Customer implements MultiTenant<Customer>
{
    boolean isactive
    boolean is_subscribed_to_newsletter
    String tax_number   //GST Number
    double height_major_metric
    double height_minor_metric
    double weight
    String designation
    String about
    String geolocation

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,user:User,person:Person,
                      customegroup: CustomerGroup,measurementmetrictype:MeasurementMetricType,createdvia:AppType]
    static constraints = {
        tax_number nullable:true
        designation nullable:true
        about nullable:true
        geolocation nullable:true
        customegroup nullable:true
        measurementmetrictype nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        is_subscribed_to_newsletter defaultValue: true
        height_major_metric defaultValue: 0
        height_minor_metric defaultValue: 0
        weight defaultValue: 0
    }
}

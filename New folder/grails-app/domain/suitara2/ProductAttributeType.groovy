package suitara2
import grails.gorm.MultiTenant
class ProductAttributeType implements MultiTenant<ProductAttributeType>
{
    String name        //Color/Material/Pattern/Weight/Size
    boolean isactive
    boolean is_brand_applicable
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,category:Category]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
        is_brand_applicable defaultValue: false
    }
}

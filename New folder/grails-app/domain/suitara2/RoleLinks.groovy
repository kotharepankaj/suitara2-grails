package suitara2
import grails.gorm.MultiTenant
class RoleLinks implements MultiTenant<RoleLinks>
{
    int linkid //LinkSuperMaster
    int sort_order
    boolean isactive
    String linkname
    String linkdescription

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[role:Role,usertype:UserType,organization:Organization]
    static constraints = {
        linkname nullable:true
        linkdescription nullable:true
        role nullable:true
        usertype nullable:true
        organization nullable:true
        roletype nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        linkid defaultValue:0
        sort_order defaultValue:0
    }
}
